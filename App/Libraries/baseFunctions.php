<?php
/** return current time in 'Y-m-d H:i:s' format ( string)
 * @return $string
 */
function getNowTime()
{
    $datetime = new DateTime();
    $nowTime = $datetime->format('Y-m-d H:i:s');
    return $nowTime;
}

/** return base url
 * @return string
 */
function getBaseUrl()
{
    return '/';
}

/** returns directory where web root is ( like '/var/www' in ubuntu ) */
function getWebRootDirectory()
{
    return PROJECT_ROOT_DIR;
}

/** require once library file
 * @param string $libraryFileName*/
function requireLibrary($libraryFileName) {
    $fullLibraryFileName = __DIR__ . DIRECTORY_SEPARATOR . $libraryFileName . '.php';
    require_once($fullLibraryFileName);
}
