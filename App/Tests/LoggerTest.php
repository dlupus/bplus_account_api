<?php
require_once(__DIR__ . '/../../prelude.php');
use App\Services\Classes\Logger;
/** @var Logger $logger */
$logger = Logger::getInstance();
$logger->log('test message', 'devel' );
$dataArray = array('data 1 ' => array('data 1.1' => 'fasldfasdf'), ' data2 ' => 'data 34');
$logResult = $logger->log('message with dataArray and date ', ' test log' , $dataArray , new DateTime());
if($logResult) {
    echo "Logger test ok";
} else {
    echo " Logger test failed";
}