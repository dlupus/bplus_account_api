<?php
require_once(__DIR__ . '/../../prelude.php');
use App\Services\Classes\DefaultConfigManager;

/** @var DefaultConfigManager $configManger */
$configManger = DefaultConfigManager::getInstance();
$logParams = $configManger->get('logs');
$logDir = $configManger->get('logs/log_directory');
//var_dump($logParams);
//echo( $logDir);
if (!empty($logParams) and is_string($logDir)) {
    echo "ConfigManger test ok\n";
} else {
    echo "ConfigManger test failed\n";
}

