<?php
require_once(__DIR__ . '/../../prelude.php');
use App\Services\Classes\ServiceManager;

$logger = ServiceManager::get('logger');
$config_manager = ServiceManager::get('config_manager');
if (is_a($logger, '\API\Services\Interfaces\ILoggerService') && is_a($config_manager, '\API\Services\Interfaces\IConfigurationService')) {
    echo 'ServiceManager test ok';
} else {
    echo 'ServiceManager test failed';
}