<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 24.06.14
 * Time: 17:32
 */

namespace App\Services\Interfaces;

/** interface for classes, which can provide work with database */
interface IDBManagerService
{
    /** returns connection if exists or creats it
     * @param string $server
     * @param string $user
     * @param string $database
     * @param string $database ;
     */
    public function getConnection($server, $user, $password,  $database);

    /** make select return query result
     * @param string $sql
     * @param object $connection
     */
    public function executeSql($sql,$connection);
}