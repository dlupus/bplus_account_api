<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 10.06.14
 * Time: 16:29
 */

namespace App\Services\Interfaces;


use App\Services\AbstractSingletonService;

interface IServiceManagerService
{
    /** use get function to get some service assigned to specific work in config.ini
     * @param $serviceRoleName - name of specific work which service is assigned for in config.ini
     * @return AbstractSingletonService
     */
    public static function get($serviceRoleName);
} 