<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 24.06.14
 * Time: 17:43
 */

namespace App\Services\Interfaces;

/** to manage extinsions */
interface IExtensionManagerService
{
    /** create object of extension class or require ext script ( is no class in assigned file)*/
    public function  getExtension($extensionName);

    /** returns an object of extension
     * @param string $extName - like 'oracleDB' or 'mysqlDB' assigned in config.yml
     */
    public function getExtensionObject($extensionName);

    /** require extension .php file
     * @param $extension
     */
    public function requireExtensionFile($extensionName);
}