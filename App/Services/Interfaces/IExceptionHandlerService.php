<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 24.06.14
 * Time: 18:45
 */

namespace App\Services\Interfaces;


interface IExceptionHandlerService {
    /** enable user exception Handling , ( extension handler class is assigned in config.yml )  */
    public function enableExceptionHandling();

}