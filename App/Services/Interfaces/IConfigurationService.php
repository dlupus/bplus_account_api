<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 10.06.14
 * Time: 16:14
 */

namespace App\Services\Interfaces;


interface IConfigurationService
{
    /** use function set to set some parameter ( be careful with this function , after changing parameter value it
     * will not be equal to appropriate parameter in config.ini )
     * @param array $paramArray array like 'paramName1' => 'value1', 'paramName2'=>'value2', ..
     * @return bool
     */
    public function set($paramArray);

    /** use get function to get value of parameter or parameters
     * @param array /string $paramName -name of param that you want to get it can be string or array of strings
     * @return string
     */
    public function get($paramSectionName);
}