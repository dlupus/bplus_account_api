<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 10.06.14
 * Time: 15:58
 */

namespace App\Services\Interfaces;

/** interface for classes whick can verify requests for API and check some security keys  */
interface IAPICheckService
{
    /** function verifyKey should be used to varify some key, using some parameter is security section in config.ini
     * @param string $keyParameterName shows which security parameter should bu used
     * @param string $key
     * @return bool
     */
    public function verifyKey($keyParameterName, $key);
} 