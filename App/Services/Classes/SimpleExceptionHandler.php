<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 26.06.14
 * Time: 16:35
 */

namespace App\Services\Classes;


use App\Services\Interfaces\IExceptionHandlerService;

class SimpleExceptionHandler extends AbstractSingletonService implements IExceptionHandlerService {

    /** enable user exception Handling , ( extension handler class is assigned in config.yml )  */
    public function enableExceptionHandling()
    {
        echo " simple exception handler";
        $exceptionThrower = new ExceptionThrowerWrapper();
        $exceptionThrower->Start();
    }
}