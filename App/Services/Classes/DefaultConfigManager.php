<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.06.14
 * Time: 17:01
 */

namespace App\Services\Classes;


use App\Services\Interfaces\IConfigurationService;

class DefaultConfigManager extends AbstractSingletonService implements IConfigurationService
{ // put here path to config file from base url
    protected $configFile = 'config.ini';
    /** @var  array $parameters */
    protected $parameters;

    /** use function set to set some parameter ( be careful with this function , after changing parameter value it
     * will not be equal to appropriate parameter in config.ini )
     * @param array $paramArray array like 'paramName1' => 'value1', 'paramName2'=>'value2', ..
     * @return bool
     */
    public function set($paramArray)
    {
        // TODO: Implement set() method.
    }

    /** use get function to get value of parameters
     * @param array /string $paramFullName -name of section in config.ini file example 'logs' or 'api'
     *
     * @return array/string
     */
    public function get($paramFullName)
    {
        //Todo: реализовать получение вложенных массивов с параметрами

        if (empty($this->parameters)) {
            $this->parseParameterFile($this->getConfigFilePath());
            ServiceManager::get('logger')->log('parameters parsed', 'dev', array('parameters' => $this->parameters));
        }
        $parameters = array();
        $paramFullName = mb_strtoupper($paramFullName);
        foreach (is_array($paramFullName) ? $paramFullName : array($paramFullName) as $paramFullName) {
            $paramAr = explode('/', $paramFullName);
            $paramSectionName = $paramAr[0];
            $paramName = false;
            if (isset($paramAr[1])) {
                $paramName = mb_strtolower($paramAr[1]);
            }
            $paramSectionName = mb_strtoupper($paramSectionName);
            if (isset($this->parameters[$paramSectionName])) {
                $paramSection = $this->parameters[$paramSectionName];
                if ($paramName && isset($paramSection[$paramName])) {

                    $parameters[$paramFullName] = $paramSection[$paramName];
                } else if ($paramName) {
                    throw new \Exception("can't get parameter " . $paramName . " from section " . $paramSection);
                } else {
                    $parameters[$paramSectionName] = $paramSection;
                }

            } else {
                ServiceManager::get('logger')->log("can't get parameter section " . $paramSectionName . "it is not set", 'error', array('params' => $this->parameters));
                var_dump($this->parameters);
                throw new \Exception("can't get parameter section " . $paramSectionName . " it is not set");
            }

        }
        if (!is_array($paramFullName) and count($parameters) === 1) {
            $parameters = $parameters[$paramFullName];
        }
        return $parameters;
    }

    /** function parseParameter File parses config.ini file and return associative array with parameters
     * @param $configFile - full path to config file
     * @return mixed
     */
    protected function parseParameterFile($configFile)
    {
        if ($params = parse_ini_file($configFile, true)) {
            $this->parameters = $params;
        } else {
            throw new  \Exception("Can't parse config file : " . $configFile);
        }
    }

    /** function return full path to file with configuration
     * @return string
     */
    protected function getConfigFilePath()
    {
        $configFilePath = getWebRootDirectory() . DIRECTORY_SEPARATOR . $this->configFile;
        if (!file_exists($configFilePath)) {
            throw new \Exception(" can't find config file : " . $configFilePath);
        }
        return $configFilePath;
    }
}