<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.06.14
 * Time: 16:57
 */

namespace App\Services\Classes;

use App\Services\Interfaces\IAPICheckService;

/** objects of this class checks API key from request to filter unwanted requests */
class APIKeyChecker extends AbstractSingletonService implements IAPICheckService
{
    /** verifyKeyAndHost
     * @param array $keyAr
     * @param string $origin
     * @rerutn bool
     */
    public function verifyKeyAndHost($keyAr)
    {
        $access = true;
        foreach ($keyAr as $keyName => $keyValue) {
            if (!$this->verifyKey($keyName, $keyValue)) {
                $access = false;
            }
        }
        if (!$this->verifyHost()) {
            $access = false;
        }
        if (!$access) {
            ServiceManager::get('logger')
                ->log('access denied', 'access', $this->getUserInfoArray());
            ServiceManager::get('logger')
                ->log('access denied', 'error', $this->getUserInfoArray());
            return false;
        } else {
            ServiceManager::get('logger')
                ->log('access allowed', 'access', $this->getUserInfoArray());
            return true;
        }
    }

    /** verify hosts
     * @return bool
     */
    public function verifyHost()
    {
        $allowedHosts = ServiceManager::get('config_manager')->get('allowed_hosts');
        $userInfoArray = $this->getUserInfoArray();
        $origin = $userInfoArray['IP'];
        if (!in_array($origin, $allowedHosts)) {
            ServiceManager::get('logger')
                ->log($origin . ' is verified ' . 'result : failed', 'access');
            ServiceManager::get('logger')
                ->log($origin . ' is verified ' . 'result : failed', 'error');
            return false;
        } else {
            ServiceManager::get('logger')
                ->log($origin . ' is verified ' . 'result : success', 'access');
            return true;
        }

    }

    /** function verifyKey should be used to varify some key, using some parameter is security section in config.ini
     * @param string $keyParameterName shows which security parameter should bu used
     * @param string $key
     * @return bool
     */
    public function verifyKey($keyParameterName, $key)
    {
        $rightKey = ServiceManager::get('config_manager')->get("security/$keyParameterName");
        $userInfoArray = $this->getUserInfoArray();
        if ($key === $rightKey) {
            ServiceManager::get('logger')
                ->log($keyParameterName . ' is verified ' . 'result : success', 'access', $userInfoArray);
            return true;
        } else {
            ServiceManager::get('logger')
                ->log($keyParameterName . ' is verified ' . 'result : failed', 'access', $userInfoArray);
            ServiceManager::get('logger')
                ->log($keyParameterName . ' is verified ' . 'result : failed', 'error', $userInfoArray);
            return false;
        }
    }

    protected function getUserInfoArray()
    {
        $userInfoArray = array(
            'IP' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'NO REMOTE ADDRESS',
            'REMOTE_HOST' => isset($_SERVER['REMOTE_HOST']) ? $_SERVER['REMOTE_HOST'] : 'NO REMOTE HOST'
        );
        return $userInfoArray;
    }
}