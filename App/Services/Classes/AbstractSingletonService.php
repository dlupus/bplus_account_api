<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.06.14
 * Time: 17:04
 */

namespace App\Services\Classes;
require_once(__DIR__ . '/../../Libraries/baseFunctions.php');

class AbstractSingletonService
{
    //instance of the object
    /** @var  array $instances */
    private static $instances;

    //disable from "new" operation
    private function __construct()
    {
    }

    //disable __clone() operation
    private function __clone()
    {
    }

    //disable unserialise
    private function __wakeup()
    {
    }

    // get instance of Service
    public static function getInstance()
    {
        $calledClass = get_called_class();
        if (empty(self::$instances[$calledClass])) {
            self::$instances[$calledClass] = new $calledClass;
        }
        return self::$instances[$calledClass];
    }
}
