<?php
require_once('prelude.php');
use App\Services\Classes\ServiceManager;
use App\AppClasses\API\APIEngine;

header('Content-type: text/html; charset=UTF-8');
ServiceManager::get('exception_handler')->enableExceptionHandling();

if (count($_REQUEST) > 0) {
    try {
        ServiceManager::get('logger')->log('has api request', 'dev', array('request' => $_REQUEST['request']));
        /** @var APIEngine $APIEngine */
        $APIEngine = new APIEngine($_REQUEST);
        echo $APIEngine->callApiFunction();
    } catch
    (\Exception $e) {

        ServiceManager::get('logger')->log(' error : ' . $e->getMessage(), 'error', array(' stack trace : ' => $e->getTraceAsString()));

        header("Content-Type: application/json");
        header('HTTP/1.1  500 Internal Server Error');
        echo json_encode(Array('error' => 'internal api error'));
        exit();
    }
} else {
    ServiceManager::get('logger')->log('wrong url !', 'dev');
    throw new  Exception("wrong url !");
}
?>
